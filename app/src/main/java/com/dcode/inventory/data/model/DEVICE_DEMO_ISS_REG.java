package com.dcode.inventory.data.model;

import java.io.Serializable;

public class DEVICE_DEMO_ISS_REG  implements Serializable  {

    public int ID;
    public String EMP_RESPONSIBLE;
    public String CLIENT_NAME;
    public String ISSUE_DATE;
    public String EXP_RET_DATE;
    public String PART_NO;
    public String SL_NO;
    public int IS_RETURNED;
    public String RET_DATE;
    public String REMARKS;
    public int QTY;
    public String DEVICE_DESC;
    public int DUE_DAYS;


}