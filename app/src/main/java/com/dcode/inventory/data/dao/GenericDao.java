package com.dcode.inventory.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;
import com.dcode.inventory.data.model.APP_SETTINGS;
import com.dcode.inventory.data.model.GENERIC_COUNT;
import com.dcode.inventory.data.model.MAX_VALUE;
import com.dcode.inventory.data.model.ORTYP;
import com.dcode.inventory.data.model.USERS;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GenericDao {

    @RawQuery
    MAX_VALUE getMaxValue(SupportSQLiteQuery query);

    @RawQuery
    GENERIC_COUNT getCount(SupportSQLiteQuery query);

    @Query("DELETE FROM OBJECTS WHERE MASTER_ID=:master_id")
    void deleteAllMasters(long master_id);

    @Query("DELETE FROM APP_SETTINGS")
    void deleteAllAppSettings();

    @Insert(onConflict = REPLACE)
    void insertAllAppSettings(APP_SETTINGS... app_settings);

    @Query("SELECT * FROM APP_SETTINGS ORDER BY ID DESC LIMIT 1")
    APP_SETTINGS getAppSettings();



    // region " USERS "

    @Query("SELECT * FROM USERS LIMIT 1")
    USERS getAllUsers();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:userName AND USER_PWD=:userPassword LIMIT 1")
    USERS validateUser(String userName, String userPassword);

    @Insert(onConflict = REPLACE)
    void insertUser(USERS... user);

    @Query("DELETE FROM USERS WHERE USER_ID=:userID")
    void deleteUser(String userID);

    @Query("DELETE FROM USERS")
    void deleteAllUser();

    @Query("SELECT * FROM USERS WHERE USER_NAME=:loginName AND USER_PWD=:passWd")
    USERS getUser(String loginName, String passWd);



    @Query("DELETE FROM ORTYP")
    void deleteAllOrTyps();

    @Insert(onConflict = REPLACE)
    void insertOrTyps(ORTYP... ortyps);



}

