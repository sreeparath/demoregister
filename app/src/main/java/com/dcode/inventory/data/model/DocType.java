package com.dcode.inventory.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "DOCTYPE")
public class DocType implements Serializable {
    @PrimaryKey
    @ColumnInfo(name = "CODE")
    @NotNull
    public String CODE;

    @ColumnInfo(name = "NAME")
    public String NAME;

    @ColumnInfo(name = "ID")
    public String ID;

    @ColumnInfo(name = "BIN_ID")
    public int BIN_ID;

    @Override
    public String toString() {
        return NAME;
    }
}
