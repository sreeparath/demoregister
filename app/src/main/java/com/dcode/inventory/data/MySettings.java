package com.dcode.inventory.data;

import com.dcode.inventory.App;
import com.dcode.inventory.data.model.APP_SETTINGS;

public class MySettings {
    private static MySettings mySettings;
    private APP_SETTINGS appSettings;
    private String BASE_URL;

    private MySettings() {
        if (appSettings == null || appSettings.PK_ID <= 0) {
            appSettings = App.getDatabaseClient().getAppDatabase().genericDao().getAppSettings();
        }
    }

    public static synchronized MySettings getInstance() {
        if (mySettings == null) {
            mySettings = new MySettings();
        }

        return mySettings;
    }

    public String getBaseUrl() {
        return BASE_URL;
    }

    public void setBaseUrl(String baseUrl) {
        BASE_URL = baseUrl;
    }

    public void destroyInstance() {
        mySettings = null;
    }

    public boolean IsGroupVisible() {
        return appSettings.IS_GROUP_VISIBLE == 1;
    }

    public boolean IsSubGroupVisible() {
        return appSettings.IS_SUB_GROUP_VISIBLE == 1;
    }

    public boolean IsCategoryVisible() {
        return appSettings.IS_CATEGORY_VISIBLE == 1;
    }

    public boolean IsSubCategoryVisible() {
        return appSettings.IS_SUB_CATEGORY_VISIBLE == 1;
    }

    public boolean IsRoomVisible() {
        return appSettings.IS_ROOM_VISIBLE == 1;
    }

    public boolean IsSubGroupIn() {
        return appSettings.IS_SUB_CATEGORY_IN == 1;
    }

    public boolean IsSubCategoryIn() {
        return appSettings.IS_SUB_CATEGORY_IN == 1;
    }
}
