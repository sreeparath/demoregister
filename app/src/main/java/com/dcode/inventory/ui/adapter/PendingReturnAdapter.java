package com.dcode.inventory.ui.adapter;


import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DEVICE_DEMO_ISS_REG;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PendingReturnAdapter
        extends RecyclerView.Adapter<PendingReturnAdapter.RecyclerViewHolder>
        implements Filterable {
    private List<DEVICE_DEMO_ISS_REG> plItemsList;
    private List<DEVICE_DEMO_ISS_REG> plItemsListFull;
    private View.OnClickListener shortClickListener;
    private Filter modelFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DEVICE_DEMO_ISS_REG> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(plItemsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (DEVICE_DEMO_ISS_REG item : plItemsListFull) {
                    if ((item.SL_NO != null && item.SL_NO.toLowerCase().contains(filterPattern)) ||
                            (item.PART_NO != null && item.PART_NO.toLowerCase().contains(filterPattern)) ||
                            (item.DEVICE_DESC != null && item.DEVICE_DESC.toLowerCase().contains(filterPattern))
                    ) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            plItemsList.clear();
            if (results != null && results.values != null) {
                plItemsList.addAll((List) results.values);
            }
            notifyDataSetChanged();
        }
    };

    public PendingReturnAdapter(List<DEVICE_DEMO_ISS_REG> dataList, View.OnClickListener shortClickListener) {
        this.plItemsList = dataList;
        this.shortClickListener = shortClickListener;
    }

    @Override
    public Filter getFilter() {
        return modelFilter;
    }

    @NonNull
    @Override
    public PendingReturnAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_pl_items, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull PendingReturnAdapter.RecyclerViewHolder holder, int position) {
        final DEVICE_DEMO_ISS_REG ls_hdr = plItemsList.get(position);
        holder.tvName.setText(ls_hdr.DEVICE_DESC);
        holder.tvSlNo.setText(ls_hdr.SL_NO);
        holder.tvPartNo.setText(String.valueOf(ls_hdr.PART_NO));
        holder.tvIssue.setText(ls_hdr.ISSUE_DATE);
        holder.tvExpRet.setText(ls_hdr.EXP_RET_DATE);
        holder.tvDue.setText(String.valueOf(ls_hdr.DUE_DAYS));
        holder.tvResEmp.setText(ls_hdr.EMP_RESPONSIBLE);

        holder.itemView.setTag(ls_hdr);
        holder.itemView.setOnClickListener(shortClickListener);

//        holder.btnAdvice.setVisibility(View.INVISIBLE);
//        holder.btnAdvice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_hdr);
//                Navigation.findNavController(view).navigate(R.id.nav_load_slip_list_advice,bundle);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return plItemsList.size();
    }

    public void addItems(List<DEVICE_DEMO_ISS_REG> dataList) {
        this.plItemsList = new ArrayList<>(dataList);
        this.plItemsListFull = new ArrayList<>(dataList);

        notifyDataSetChanged();
    }

    private OnItemClickListener mListener;
    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    static class RecyclerViewHolder
            extends RecyclerView.ViewHolder  {
        private TextView tvSlNo;
        private TextView tvPartNo;
        private TextView tvIssue;
        private TextView tvExpRet;
        private TextView tvName;
        private TextView tvDue;
        private TextView tvResEmp;

        RecyclerViewHolder(View view) {
            super(view);
            tvSlNo = view.findViewById(R.id.tvSlNo);
            tvPartNo = view.findViewById(R.id.tvPartNo);
            tvIssue = view.findViewById(R.id.tvIssue);
            tvExpRet = view.findViewById(R.id.tvExpRet);
            tvName = view.findViewById(R.id.tvName);
            tvDue = view.findViewById(R.id.tvDue);
            tvResEmp = view.findViewById(R.id.tvResEmp);
        }
    }
}
