package com.dcode.inventory.ui.view;

import android.os.Bundle;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppPreferences;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class SettingsActivity extends BaseActivity {
    private TextInputEditText edServerURL;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_settings;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        MaterialButton btnNext = findViewById(R.id.btnNext);
        edServerURL = findViewById(R.id.edServerURL);
        TextInputEditText tvDeviceID = findViewById(R.id.tvDeviceID);

        btnNext.setOnClickListener(v -> onSave());

        tvDeviceID.setText(AppPreferences.getValue(SettingsActivity.this, AppPreferences.DEV_UNIQUE_ID, true));

        String ServiceURL = AppPreferences.getValue(SettingsActivity.this, AppPreferences.SERVICE_URL, false);
        if (ServiceURL.length() <= 0)
            ServiceURL = getString(R.string.default_url);
        edServerURL.setText(ServiceURL);
    }

    private void onSave() {
        showToast("Restart application for setting to apply");
        String ServiceURL = edServerURL.getText().toString();
        AppPreferences.SetValue(SettingsActivity.this, AppPreferences.SERVICE_URL, ServiceURL);
        App.appSettings().setBaseUrl(ServiceURL);
        App.getNetworkClient().destroyInstance();
        finish();
    }
}
