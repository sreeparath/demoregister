package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DEVICE_DEMO_ISS_REG;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.FIND_BATCH;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CheckInFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    DEVICE_DEMO_ISS_REG demo_iss_reg;
    FIND_BATCH picklist_item_rec;
    List<FIND_BATCH> picklist_item_recList;
    private TextInputEditText edSlNo;
    private TextInputEditText edPartNo;
    private TextInputEditText edRepEmp;
    private TextInputEditText edClient;
    private TextInputEditText edIssueDt;
    private TextInputEditText edExpRetDt;
    private TextInputEditText edRemarks;
    private TextInputEditText edDesc;
    private View root;
    private AutoCompleteTextView tvUnits;
    private ArrayAdapter<DocType> objectsAdapter;
    private DocType unitType;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
               demo_iss_reg = (DEVICE_DEMO_ISS_REG) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_checkin, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(demo_iss_reg!=null){
            demo_iss_reg.ISSUE_DATE = Utils.getFormattedDate(demo_iss_reg.ISSUE_DATE,Utils.DB_DATE_FORMAT2,Utils.PRINT_DATE_FORMAT);
            demo_iss_reg.EXP_RET_DATE = Utils.getFormattedDate(demo_iss_reg.EXP_RET_DATE,Utils.ISSUE_DATE_FORMAT,Utils.PRINT_DATE_FORMAT);
            fillBatch(demo_iss_reg);
            edRemarks.requestFocus();
        }
    }

    private void setupView() {
        edSlNo = root.findViewById(R.id.edSlNo);
        edSlNo = root.findViewById(R.id.edSlNo);
        edPartNo = root.findViewById(R.id.edPartNo);
        edPartNo.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }

        });

        edDesc = root.findViewById(R.id.edDesc);
        edDesc.setEnabled(false);
        edRepEmp = root.findViewById(R.id.edRepEmp);
        edRepEmp.setEnabled(false);
        edClient = root.findViewById(R.id.edClient);
        edClient.setEnabled(false);
        edIssueDt = root.findViewById(R.id.edIssueDt);
        edIssueDt.setEnabled(false);
        edExpRetDt = root.findViewById(R.id.edExpRetDt);
        edExpRetDt.setEnabled(false);
        edIssueDt.setOnClickListener(v -> onDateEntryClick(1));
        edExpRetDt.setOnClickListener(v -> onDateEntryClick(2));
        edRemarks = root.findViewById(R.id.edRemarks);
        edRemarks.setEnabled(false);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onAddClick());


        MaterialButton btnClose = root.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(v -> onCloseClick());



    }


    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edIssueDt.setText(selectedDate);
                edIssueDt.setError(null);
            } else if (ID == 2) {
                edExpRetDt.setText(selectedDate);
                edExpRetDt.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //plItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edPartNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onCloseClick() {
        getActivity().onBackPressed();
    }

    private void onAddClick() {
        if (IsDataValid()) {
            save();
            edSlNo.requestFocus();
        }
    }

    private void save() {
        DEVICE_DEMO_ISS_REG device_demo_iss_reg = new DEVICE_DEMO_ISS_REG();
        //int transId = Integer.parseInt(AppPreferences.getValue(getContext(), AppPreferences.TRANS_ID, true));
        //device_demo_iss_reg.ID = transId;
        device_demo_iss_reg.SL_NO = edSlNo.getText().toString();;
        device_demo_iss_reg.PART_NO = edPartNo.getText().toString();;
        device_demo_iss_reg.EMP_RESPONSIBLE = edRepEmp.getText().toString();;
        device_demo_iss_reg.CLIENT_NAME = edClient.getText().toString();;
        device_demo_iss_reg.ISSUE_DATE = edIssueDt.getText().toString();;
        device_demo_iss_reg.EXP_RET_DATE = edExpRetDt.getText().toString();;
        device_demo_iss_reg.IS_RETURNED = 1;
        device_demo_iss_reg.REMARKS = edRemarks.getText().toString();
        device_demo_iss_reg.QTY = 1;
        device_demo_iss_reg.DEVICE_DESC = edDesc.getText().toString();
        //transId++;
        //AppPreferences.SetValue(getContext(), AppPreferences.TRANS_ID, String.valueOf(transId));


        UploadServer(device_demo_iss_reg);
    }

    private boolean IsDataValid() {
        String errMessage;

        String slno = edSlNo.getText().toString().trim();
        if (slno.length() == 0) {
            errMessage = "Invalid SL No";
            showToast(errMessage);
            edSlNo.setError(errMessage);
            return false;
        }

        String desc = edDesc.getText().toString().trim();
        if (desc.length() == 0) {
            errMessage = "Invalid Device Desc";
            showToast(errMessage);
            edDesc.setError(errMessage);
            return false;
        }

        String mfd = edIssueDt.getText().toString();
        Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
        if (mfDate==null) {
            errMessage = "Invalid Issue date";
            showToast(errMessage);
            edIssueDt.setError(errMessage);
            return false;
        }

        String exp = edExpRetDt.getText().toString();
        Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
        if (exDate==null) {
            errMessage = "Invalid Expected Return date";
            showToast(errMessage);
            edExpRetDt.setError(errMessage);
            return false;
        }





        return true;
    }


    private void UploadServer(DEVICE_DEMO_ISS_REG device_demo_iss_reg) {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.RegisterDemo.getRegisterData(device_demo_iss_reg);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        showAlert("Upload Success",msg);
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
                       resetUI();
                        edSlNo.setText("");
                        edSlNo.requestFocus();
                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }


    private void ParseBarCode(String scanCode) {
        resetUI();
        String slNo  =edSlNo.getText().toString();
        if (slNo.length() <= 0  ) {
            showToast("Scan SL No first!");
            return;
        }
        if (scanCode.length() <= 0  ) {
            return;
        }
        findBatch(slNo,scanCode);
        //edScanCode.setText("");
        //edScanCode.requestFocus();
    }

    private void resetUI(){
        edPartNo.setText("");
        edRepEmp.setText("");
        edClient.setText("");
        edIssueDt.setText("");
        edExpRetDt.setText("");
        edRemarks.setText("");
        edDesc.setText("");
    }

    private void fillBatch(DEVICE_DEMO_ISS_REG find_batch){
        edPartNo.setText(find_batch.PART_NO);
        edRepEmp.setText(find_batch.EMP_RESPONSIBLE);
        edClient.setText(find_batch.CLIENT_NAME);
        edExpRetDt.setText(find_batch.EXP_RET_DATE);
        edRemarks.setText(find_batch.REMARKS);
        edDesc.setText(find_batch.DEVICE_DESC);
        edSlNo.setText(find_batch.SL_NO);
        edIssueDt.setText(find_batch.ISSUE_DATE);
    }

    private void findBatch(final String slNo, final String partNo) {
        JsonObject requestObject =  ServiceUtils.RegisterDemo.getItemCheckOut(slNo, partNo);

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        DEVICE_DEMO_ISS_REG[] item = new Gson().fromJson(xmlDoc, DEVICE_DEMO_ISS_REG[].class);
                        if (item==null || item.length<=0) {
                            showAlert(" Error"," No Item check out with this Serial and part number : "+ slNo + " "+partNo);
                            edSlNo.setText("");
                            edPartNo.setText("");
                            edSlNo.requestFocus();
                            return;
                        }
                        fillBatch(item[0]);
                        Log.d("data##",xmlDoc.toString());
                        //edScanCode.requestFocus();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

}
