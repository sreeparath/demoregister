package com.dcode.inventory.ui.view;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.data.model.DASH_SUMMARY;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeFragment
        extends BaseFragment {

    View root;
    MaterialButton edPending,edCheckOut;
    //TextToSpeech  tts;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

//        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
//            @Override
//            public void onInit(int i) {
//                if (i == TextToSpeech.SUCCESS) {
//                    int result = tts.setLanguage(Locale.UK);
//                    if (result == TextToSpeech.LANG_MISSING_DATA ||
//                            result == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "Lenguage not supported");
//                    }
//                } else {
//                    Log.e("TTS", "Initialization failed");
//                }
//            }
//        });

        root = inflater.inflate(R.layout.fragment_home, container, false);

//        TextView tv_bg_goods_receipt = root.findViewById(R.id.tv_bg_goods_receipt);
//        tv_bg_goods_receipt.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_find_batch));
//        tv_bg_goods_receipt.setText("dsdffs");

        edCheckOut = root.findViewById(R.id.edCheckOut);
        edCheckOut.setOnClickListener(v -> {
            //tts.speak("openING Out LIST?", TextToSpeech.QUEUE_FLUSH, null);
            OnModuleButtonClick(R.id.action_nav_home_to_nav_returnlist);
        });

        edPending = root.findViewById(R.id.edPending);
        edPending.setOnClickListener(v -> {
            //tts.speak("openING PENDING LIST ?", TextToSpeech.QUEUE_FLUSH, null);
            OnModuleButtonClick(R.id.action_nav_home_to_nav_pendList);
        });



        TextView tv_bg_checkout = root.findViewById(R.id.tv_bg_checkout);
        tv_bg_checkout.setOnClickListener(v -> {
            //tts.speak("openING CHECK OUT ?", TextToSpeech.QUEUE_FLUSH, null);
            OnModuleButtonClick(R.id.action_nav_home_to_nav_checkout);
        });

        TextView tv_bg_checkin = root.findViewById(R.id.tv_bg_checkin);
        tv_bg_checkin.setOnClickListener(v -> {
            //tts.speak("openING CHECK IN ?", TextToSpeech.QUEUE_FLUSH, null);
            OnModuleButtonClick(R.id.action_nav_home_to_nav_checkin);
        });



        TextView tv_bg_settings = root.findViewById(R.id.tv_bg_settings);
        tv_bg_settings.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_settings));



        TextView tv_bg_logout = root.findViewById(R.id.tv_bg_logout);
        tv_bg_logout.setOnClickListener(v -> OnLogoutClick());

        getSummaryData();
        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void OnLogoutClick() {
        ((MainActivity) requireActivity()).SignOut();
    }

    private void getSummaryData() {
        JsonObject requestObject = ServiceUtils.RegisterDemo.getSummary();

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (!(xmlDoc != null || xmlDoc.trim().length() != 0)) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        Log.d("data## ", xmlDoc);
                        DASH_SUMMARY[] ls_hdr = new Gson().fromJson(xmlDoc, DASH_SUMMARY[].class);
                        if (ls_hdr==null || ls_hdr.length<=0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        edPending.setText("PENDING : " +String.valueOf(ls_hdr[0].TOTAL_PEND));
                        edCheckOut.setText("OUT : " +String.valueOf(ls_hdr[0].TOTAL_OUT));
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }


}
