package com.dcode.inventory.ui.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DEVICE_DEMO_ISS_REG;
import com.dcode.inventory.data.model.SEARCH_TYPE;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.dcode.inventory.ui.adapter.PendingReturnAdapter;
import com.dcode.inventory.ui.adapter.ReturnAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ReturnFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {


    List<DEVICE_DEMO_ISS_REG> pendingLists;
    DEVICE_DEMO_ISS_REG device_demo_iss_reg;
    //private TextInputEditText edRefNo;
    //private TextInputEditText edRefDate;
    private TextInputEditText edVehicle;
    private TextInputEditText edDriver;
    private TextInputEditText edHelper;
    private TextInputEditText edRemarks;
    private TextInputEditText edReceiptNo;
    private TextInputEditText edSearch;
    private View root;
    private ReturnAdapter pendingReturnAdapter;
    private FloatingActionButton proceed;
    private TextInputEditText edPostRefNo;
    private TextInputEditText edPostRemarks;
    private MaterialButton btnPostReceipt;
    private SEARCH_TYPE sLoc_Type;
    private AutoCompleteTextView orderBy;
    private ArrayAdapter<String> orderByAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
               // pl_hdr = (STORE_TRANSFER_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment__return, container, false);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        pendingReturnAdapter = new ReturnAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(pendingReturnAdapter);

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pendingReturnAdapter.getFilter().filter(s);

            }
        });

        DownloadReturns();
        //loadData();
    }

    private void setupView() {
//        edScanCode = root.findViewById(R.id.edScanCode);
//        edScanCode.setOnKeyListener((v, keyCode, event) -> {
//            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                    keyCode == KeyEvent.KEYCODE_ENTER) {
//                return onScanCodeKeyEvent();
//            }
//            return false;
//        });
        edSearch = root.findViewById(R.id.search);
        edSearch.requestFocus();
//        proceed = (FloatingActionButton) root.findViewById(R.id.proceed);
//        proceed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Bundle bundle = new Bundle();
//                bundle.putInt(AppConstants.MODULE_ID, ModuleID);
//                bundle.putSerializable(AppConstants.SELECTED_OBJECT, pl_hdr);
//
//                ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_store_trans_receipt, bundle);
//            }
//        });

        //edRefNo = root.findViewById(R.id.edRefNo);
        //edRefDate = root.findViewById(R.id.edRefDate);

//        edRefDate.setOnClickListener(v -> {
//            Calendar currentDate = Calendar.getInstance();
//            int curYear = currentDate.get(Calendar.YEAR);
//            int curMonth = currentDate.get(Calendar.MONTH);
//            int curDay = currentDate.get(Calendar.DAY_OF_MONTH);
//
//            DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
//                String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
//                String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);
//
//                edRefDate.setText(selectedDate);
//            }, curYear, curMonth, curDay);
//            mDatePicker.setTitle("Select date");
//            mDatePicker.show();
//        });





        btnPostReceipt = root.findViewById(R.id.btnSave);
        btnPostReceipt.setOnClickListener(v -> getActivity().onBackPressed());

//        orderBy = root.findViewById(R.id.edOrder);
//        orderBy.setVisibility(View.GONE);
//        setOrderBy();
//        orderBy.setOnItemClickListener((parent, view, position, id) -> {
//            //showToast("position"+String.valueOf(position));
//            if (position >= 0) {
//                if (position == 0) {
//                    loadData("SL_NO");
//                } else if (position == 1) {
//                    loadData("REC");
//                } else if (position == 2) {
//                    loadData("PEND");
//                }
//            }
//        });

}


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        pendingReturnAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
        DEVICE_DEMO_ISS_REG demo_items = (DEVICE_DEMO_ISS_REG) view.getTag();

        if (demo_items == null) {
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, demo_items);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_checkin, bundle);


        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/


    //private void onClickSave() {
       // SaveData();
    //}

    /*private void loadData(String orderBy) {
        if (pl_hdr == null) {
            showToast("Some error");
            return;
        }

        //pl_itemsList = App.getDatabaseClient().getAppDatabase().genericDao().GetLOAD_SLIP_ITEMS_NEW(pl_hdr.LSNO);// summary list
        pendingLists = App.getDatabaseClient().getAppDatabase().genericDao().GetSTORE_TRANSFER_ITEMS(pl_hdr.LSNO,orderBy);
        if (pendingLists != null && pendingLists.size()>0 ) {
            pendingReturnAdapter.addItems(pendingLists);
            btnPostReceipt.setEnabled(true);
            proceed.setEnabled(true);
        }
        else {
            btnPostReceipt.setEnabled(false);
            proceed.setEnabled(false);
            showToast(" Alert : No data found.");
        }
    }*/

    private void DownloadReturns() {
        showProgress(false);

        JsonObject jsonObject;
        JsonArray array = new JsonArray();
        jsonObject = ServiceUtils.createJsonObject("USER_ID", "22", App.currentUser.USER_ID);
        array.add(jsonObject);
        jsonObject = ServiceUtils.createJsonObject("CURR_DT", "22", Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT));
        array.add(jsonObject);

        JsonObject requestObject = new JsonObject();
        requestObject.addProperty("ProcName", "GET_DEMO_REG_RETURN");
        requestObject.addProperty("DBName", App.currentCompany.DbName);
        requestObject.add("dbparams", array);

        System.out.println("requestObject##"+requestObject.toString());

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        DEVICE_DEMO_ISS_REG[] pl_items = new Gson().fromJson(xmlDoc, DEVICE_DEMO_ISS_REG[].class);
                        pendingLists = Arrays.asList(pl_items);
                        pendingReturnAdapter.addItems(pendingLists);
                        //loadData("SL_NO");
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }


    private void resetUI(){

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == AppConstants.SLOC) {
            sLoc_Type = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (SEARCH_TYPE) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (sLoc_Type != null) {
                edDriver.setText(sLoc_Type.NAME);
            } else {
                edDriver.setText("");
            }
        }else if (requestCode == AppConstants.HELPER) {
            sLoc_Type = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (SEARCH_TYPE) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (sLoc_Type != null) {
                edHelper.setText(sLoc_Type.NAME);
            } else {
                edHelper.setText("");
            }
        }else if (requestCode == AppConstants.VEH) {
            sLoc_Type = intent.hasExtra(AppConstants.SELECTED_OBJECT) ? (SEARCH_TYPE) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT) : null;
            if (sLoc_Type != null) {
                edVehicle.setText(sLoc_Type.NAME);
            } else {
                edVehicle.setText("");
            }
        }
    }

    private void setOrderBy() {
        ArrayList<String> orderList = new ArrayList<String>();
        orderList.add("Default");
        orderList.add("Receipt");
        orderList.add("Pending");

        orderByAdapter = new ArrayAdapter<>(requireContext(), R.layout.dropdown_menu_popup_item, orderList);
        orderBy.setAdapter(orderByAdapter);
        orderBy.setThreshold(100);
    }

}
