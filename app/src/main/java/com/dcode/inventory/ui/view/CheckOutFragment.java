package com.dcode.inventory.ui.view;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.inventory.App;
import com.dcode.inventory.R;
import com.dcode.inventory.common.AppConstants;
import com.dcode.inventory.common.AppPreferences;
import com.dcode.inventory.common.Utils;
import com.dcode.inventory.data.model.DEVICE_DEMO_ISS_REG;
import com.dcode.inventory.data.model.DocType;
import com.dcode.inventory.data.model.FIND_BATCH;
import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.service.ServiceUtils;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CheckOutFragment
        extends BaseFragment
        implements View.OnClickListener,
        SearchView.OnQueryTextListener {

    //PICK_HDR pl_hdr;
    FIND_BATCH picklist_item_rec;
    List<FIND_BATCH> picklist_item_recList;
    private TextInputEditText edSlNo;
    private TextInputEditText edPartNo;
    private TextInputEditText edRepEmp;
    private TextInputEditText edClient;
    private TextInputEditText edIssueDt;
    private TextInputEditText edExpRetDt;
    private TextInputEditText edRemarks;
    private TextInputEditText edDesc;
    private View root;
    private AutoCompleteTextView tvUnits;
    private ArrayAdapter<DocType> objectsAdapter;
    private DocType unitType;
    private Drawable icon;
    private final ColorDrawable background = new ColorDrawable(Color.RED);
    private float rcd_qty;
    private TextToSpeech tts;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);

            Bundle bundle = this.getArguments();
            if (bundle != null) {
               // pl_hdr = (PICK_HDR) bundle.getSerializable(AppConstants.SELECTED_OBJECT);
            }
        }

        root = inflater.inflate(R.layout.fragment_checkout, container, false);
        icon = getResources().getDrawable(R.drawable.ic_delete_black);
        setupView();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //loadData();

    }

    private void setupView() {



        edSlNo = root.findViewById(R.id.edSlNo);
        edDesc = root.findViewById(R.id.edDesc);
//        edScanCode.setOnKeyListener((v, keyCode, event) -> {
//            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                    keyCode == KeyEvent.KEYCODE_ENTER) {
//                return onScanCodeKeyEvent();
//            }
//            return false;
//        });
        edSlNo = root.findViewById(R.id.edSlNo);
        edPartNo = root.findViewById(R.id.edPartNo);
        edRepEmp = root.findViewById(R.id.edRepEmp);
        edClient = root.findViewById(R.id.edClient);
        edIssueDt = root.findViewById(R.id.edIssueDt);
        edExpRetDt = root.findViewById(R.id.edExpRetDt);
        edIssueDt.setOnClickListener(v -> onDateEntryClick(1));
        edExpRetDt.setOnClickListener(v -> onDateEntryClick(2));
        edRemarks = root.findViewById(R.id.edRemarks);
        MaterialButton btnNext = root.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> onAddClick());


        MaterialButton btnClose = root.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(v -> onCloseClick());



    }


    private void onDateEntryClick(int ID) {
        Calendar currentDate = Calendar.getInstance();
        int curYear = currentDate.get(Calendar.YEAR);
        int curMonth = currentDate.get(Calendar.MONTH);
        int curDay = currentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(getContext(), (datePicker, cYear, cMonth, cDay) -> {
            String dateString = String.format(Locale.ENGLISH, "%d-%d-%d", cDay, (cMonth + 1), cYear);
            String selectedDate = Utils.getFormattedDate(dateString, Utils.ISSUE_DATE_FORMAT, Utils.PRINT_DATE_FORMAT);

            if (ID == 1) {
                edIssueDt.setText(selectedDate);
                edIssueDt.setError(null);
            } else if (ID == 2) {
                edExpRetDt.setText(selectedDate);
                edExpRetDt.setError(null);
            }
        }, curYear, curMonth, curDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //plItemsRecAdapter.getFilter().filter(newText);
        String filterString = newText;
        return false;
    }

    @Override
    public void onClick(View view) {
//        LS_ITEMS ls_items = (LS_ITEMS) view.getTag();
//
//        if (ls_items == null) {
//            return;
//        }

        //NavigateToItemDetails(ls_items, "");
    }

    /*private void NavigateToItemDetails(LS_ITEMS ls_items, String scanCode) {
        if (App.ls_hdr_id <= 0) {
            slip_hdr = new SLIP_HDR();
            slip_hdr.GUID = Utils.GetGUID();
            slip_hdr.IS_UPLOADED = 0;
            slip_hdr.ORDNO = ls_hdr.LSNO;
            slip_hdr.ORTYP = ls_hdr.ORTYP;
            slip_hdr.YRCD = ls_hdr.YRCD;
            slip_hdr.LOCODE = App.currentLocation.LOCODE;
            slip_hdr.PRICING = ls_hdr.PRICING;

            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_HDR(ls_hdr);;
            LS_ITEMS[] array = new LS_ITEMS[ls_itemsList.size()];
            ls_itemsList.toArray(array);
            App.getDatabaseClient().getAppDatabase().genericDao().insertLS_ITEMS(array);

            long HDR_ID = App.getDatabaseClient().getAppDatabase().genericDao().insertSlip_HDR(slip_hdr);
            App.ls_hdr_id = HDR_ID;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
        bundle.putString(AppConstants.SELECTED_CODE, scanCode);

        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_load_slip_item_detail, bundle);
    }*/

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edSlNo.getText().toString();

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void onCloseClick() {
        getActivity().onBackPressed();

    }

    private void onAddClick() {
        if (IsDataValid()) {
            save();
            edSlNo.requestFocus();
        }
    }

    private void save() {
        DEVICE_DEMO_ISS_REG device_demo_iss_reg = new DEVICE_DEMO_ISS_REG();
        //int transId = Integer.parseInt(AppPreferences.getValue(getContext(), AppPreferences.TRANS_ID, true));
        //device_demo_iss_reg.ID = transId;
        device_demo_iss_reg.SL_NO = edSlNo.getText().toString();;
        device_demo_iss_reg.PART_NO = edPartNo.getText().toString();;
        device_demo_iss_reg.EMP_RESPONSIBLE = edRepEmp.getText().toString();;
        device_demo_iss_reg.CLIENT_NAME = edClient.getText().toString();;
        device_demo_iss_reg.ISSUE_DATE = edIssueDt.getText().toString();;
        device_demo_iss_reg.EXP_RET_DATE = edExpRetDt.getText().toString();;
        device_demo_iss_reg.IS_RETURNED = 0;
        device_demo_iss_reg.REMARKS = edRemarks.getText().toString();
        device_demo_iss_reg.QTY = 1;
        device_demo_iss_reg.DEVICE_DESC = edDesc.getText().toString();
        //transId++;
        //AppPreferences.SetValue(getContext(), AppPreferences.TRANS_ID, String.valueOf(transId));


        UploadServer(device_demo_iss_reg);
    }

    private boolean IsDataValid() {
        String errMessage;

        String slno = edSlNo.getText().toString().trim();
        if (slno.length() == 0) {
            errMessage = "Invalid SL No";
            showToast(errMessage);
            edSlNo.setError(errMessage);
            return false;
        }

        String desc = edDesc.getText().toString().trim();
        if (desc.length() == 0) {
            errMessage = "Invalid Device Desc";
            showToast(errMessage);
            edDesc.setError(errMessage);
            return false;
        }

        String mfd = edIssueDt.getText().toString();
        Date mfDate = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
        if (mfDate==null) {
            errMessage = "Invalid Issue date";
            showToast(errMessage);
            edIssueDt.setError(errMessage);
            return false;
        }

        String exp = edExpRetDt.getText().toString();
        Date exDate = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
        if (exDate==null) {
            errMessage = "Invalid Expected Return date";
            showToast(errMessage);
            edExpRetDt.setError(errMessage);
            return false;
        }



        Date mfDate1 = Utils.convertStringToDate(mfd, Utils.PRINT_DATE_FORMAT);
        Date exDate1 = Utils.convertStringToDate(exp, Utils.PRINT_DATE_FORMAT);
        Date today = new Date();
        if (mfDate1.after(today)) {
            errMessage = "Invalid Issue date, date in future";
            showToast(errMessage);
            edIssueDt.setError(errMessage);
            return false;
        }

        if (exDate.before(today)) {
            errMessage = "Invalid Exp Return date, past Exp Return date";
            showToast(errMessage);
            edExpRetDt.setError(errMessage);
            return false;
        }

        return true;
    }


    private void UploadServer(DEVICE_DEMO_ISS_REG device_demo_iss_reg) {
        showToast(getString(R.string.send_data_to_server));
        JsonObject requestObject = ServiceUtils.RegisterDemo.getRegisterData(device_demo_iss_reg);
        showProgress(false);
        App.getNetworkClient().getAPIService().submitWithLines(requestObject, new Callback<GenericSubmissionResponse>() {
            @Override
            public void success(GenericSubmissionResponse genericSubmissionResponse, Response response) {
                dismissProgress();
                if (genericSubmissionResponse.getErrCode().equals("S")) {
                    if (genericSubmissionResponse.getErrCode().equals("S")) {
                        String ReceiptNo = String.valueOf(genericSubmissionResponse.getRetId());
                        //String[] msg = genericSubmissionResponse.getErrMessage().split("~");
                        String msg = genericSubmissionResponse.getErrMessage();
                        //resetUI();
                        showAlert("Upload Success",msg);
//                        if (msg.length > 1) {
//                            ReceiptNo = msg[2];
//                        }
                       resetUI();

                    } else {
                        showAlert("Error",genericSubmissionResponse.getErrMessage());
                        //showToast(genericSubmissionResponse.getErrMessage());
                    }
                } else {
                    Log.d(App.TAG, genericSubmissionResponse.getErrMessage());
                    showToast(genericSubmissionResponse.getErrMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(App.TAG, error.getMessage());
                showToast(error.getMessage());
                dismissProgress();
            }
        });
    }


    private void ParseBarCode(String scanCode) {
        resetUI();
        if (scanCode.length() <= 0  ) {
            return;
        }
        if(scanCode.length() < 5){
            return;
        }
        String itcode = scanCode.substring(0,5).toString();
        String batchNo = scanCode.substring(5,scanCode.length()).toString();
        //edItemCode.setText(itcode);
        //edBatchNo.setText(batchNo);
        findBatch(itcode,batchNo);
        //edScanCode.setText("");
        //edScanCode.requestFocus();
    }

    private void resetUI(){
        edSlNo.setText("");
        edPartNo.setText("");
        edRepEmp.setText("");
        edClient.setText("");
        edIssueDt.setText("");
        edExpRetDt.setText("");
        edRemarks.setText("");
        edDesc.setText("");
        edSlNo.requestFocus();
    }

    private void fillBatch(FIND_BATCH find_batch){
//        edMafDate.setText(find_batch.MNFDATE);
//        edExpdate.setText(find_batch.EXPDATE);
//        edDocDt.setText(find_batch.DOCDATE);
//        edDocNo.setText(find_batch.DOCNUM);
//        edOrdNo.setText(find_batch.CARDNAME);
//        edDesc.setText(find_batch.DSCRIPTION);
//        edDist.setText(find_batch.DISTNUMBER);
//        edRetDt.setText(find_batch.U_RETESTDATE);
    }

    private void findBatch(final String itemCode, final String batch) {
        JsonObject requestObject =  ServiceUtils.Batch.getFindBatch(itemCode, batch,"FINDBATCH");

        showProgress(false);
        App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
            @Override
            public void success(GenericRetResponse genericRetResponse, Response response) {
                dismissProgress();
                if (genericRetResponse.getErrCode().equals("S")) {
                    String xmlDoc = genericRetResponse.getXmlDoc();
                    if (xmlDoc == null || xmlDoc.trim().length() == 0) {
                        showToast(" Error: No data received.");
                        return;
                    }
                    try {
                        FIND_BATCH[] find_batches = new Gson().fromJson(xmlDoc, FIND_BATCH[].class);
                        if (find_batches==null || find_batches.length<=0) {
                            showToast(" Error: No data received.");
                            return;
                        }
                        fillBatch(find_batches[0]);
                        Log.d("data##",xmlDoc.toString());
                        //edScanCode.requestFocus();
                    } catch (Exception e) {
                        Log.d(App.TAG, e.toString());
                    }
                } else {
                    String msg = genericRetResponse.getErrMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                dismissProgress();
                String msg = error.getMessage();
                Log.d(App.TAG, msg);
                showToast(msg);
            }
        });
    }

}
