package com.dcode.inventory.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.inventory.R;
import com.dcode.inventory.data.model.OBJECTS;

import java.util.ArrayList;
import java.util.List;

public class ObjectsAdapter
        extends RecyclerView.Adapter<ObjectsAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<OBJECTS> dataList;
    private ArrayList<OBJECTS> dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<OBJECTS> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (OBJECTS item : dataListFull) {
                    if (item.OBJECT_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
                            item.OBJECT_CODE.toLowerCase().contains(filterPattern.toLowerCase())) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public ObjectsAdapter(Context context, List<OBJECTS> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = new ArrayList<>(objects);
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_recycleview, parent, false));
    }

    @Override
    public void onBindViewHolder(final ObjectsAdapter.RecyclerViewHolder holder, int position) {
        final OBJECTS objectItem = this.dataList.get(position);

        holder.tvCode.setText(objectItem.OBJECT_CODE);
        holder.tvName.setText(objectItem.OBJECT_NAME);

        holder.itemView.setTag(objectItem);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addItems(List<OBJECTS> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);

        notifyDataSetChanged();
    }

    public List<OBJECTS> getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvCode;
        TextView tvName;

        RecyclerViewHolder(View view) {
            super(view);
            //tvCode = view.findViewById(R.id.tvCode);
            //tvName = view.findViewById(R.id.tvName);
        }
    }

}
