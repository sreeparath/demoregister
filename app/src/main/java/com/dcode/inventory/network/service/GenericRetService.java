package com.dcode.inventory.network.service;

import com.dcode.inventory.network.model.GenericRetResponse;
import com.dcode.inventory.network.model.GenericSubmissionResponse;
import com.dcode.inventory.network.model.ValidUserResponse;
import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

public interface GenericRetService {
    @POST("/MSSQLRest.svc/ReadGenericRetJson")
    void getGenericRet(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/SaveDataForSPAndParamJson")
    void submitWithLines(@Body JsonObject objBody, Callback<GenericSubmissionResponse> callback);

    @POST("/MSSQLRest.svc/SaveDataForSPAndParam")
    void submitNoLines(@Body JsonObject objBody, Callback<GenericSubmissionResponse> callback);

    @POST("/MSSQLRest.svc/ValidateUser")
    void ValidateUser(@Body JsonObject objBody, Callback<ValidUserResponse> callback);

    @POST("/MSSQLRest.svc/SendMail")
    void SendMail(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/GetPrinterList")
    void GetPrinterList(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/GetPrintFormats")
    void GetPrintFormats(@Body JsonObject objBody, Callback<GenericRetResponse> callback);

    @POST("/MSSQLRest.svc/PrintBarcodeLabel")
    void PrintBarcodeLabel(@Body JsonObject objBody, Callback<GenericSubmissionResponse> callback);
}
