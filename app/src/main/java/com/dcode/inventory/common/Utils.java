package com.dcode.inventory.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import androidx.sqlite.db.SimpleSQLiteQuery;

import com.dcode.inventory.App;
import com.dcode.inventory.data.dao.GenericDao;
import com.dcode.inventory.data.model.CustomResult;
import com.dcode.inventory.data.model.GENERIC_COUNT;
import com.dcode.inventory.data.model.MAX_VALUE;

import java.io.File;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class Utils {
    public static final String FORMAT_WITH_DECIMAL = "###,##0.00";
    public static final String FORMAT_WITHOUT_DECIMAL = "###,##0";
    public static String DATE_FORMAT = "yyyy-MM-dd HH:mm";
    public static String DATEONLY_FORMAT = "yyyy-MM-dd";
    public static String DB_DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss a";
    public static String DATE_NUM_FORMAT = "yyyyMMdd";
    public static String ISSUE_DATE_FORMAT = "dd-MM-yyyy";
    public static String PRINT_DATE_FORMAT = "dd-MMM-yyyy";
    public static String TIME_ONLY_FORMAT = "HH:mm aa";
    public static String DB_DATE_FORMAT2 = "yyyy-MM-dd HH:mm:ss";

    private static String EXTERNAL_SD_PATH = "/storage/emulated/0/";

    public static String getExternalSdPath() {
        return EXTERNAL_SD_PATH;
    }

    public static String GetGUID() {

        String uniqueID = UUID.randomUUID().toString();
        return uniqueID;
    }

    public static long GetMaxValue(String TableName, String FieldName, boolean useFilter, String FilterFieldName, String FilterFieldValue) {
        String templateHasFilter = "SELECT IFNULL(MAX(%s),0) + 1 as MAX_VAL FROM %s WHERE %s = %s";
        String templateNoFilter = "SELECT IFNULL(MAX(%s),0) + 1 as MAX_VAL FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, FieldName, TableName, FilterFieldName, FilterFieldValue);
        } else {
            formatted = String.format(templateNoFilter, FieldName, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        MAX_VALUE objMaxVal = App.getDatabaseClient().getAppDatabase().genericDao().getMaxValue(query);
        return objMaxVal.MAX_VAL;
    }

    public static long GetMaxValue(String TableName, String FieldName, boolean useFilter, String FilterFieldName, long FilterFieldValue) {
        String templateHasFilter = "SELECT IFNULL(MAX(%s),0) as MAX_VAL FROM %s WHERE %s = %d";
        String templateNoFilter = "SELECT IFNULL(MAX(%s),0) as MAX_VAL FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, FieldName, TableName, FilterFieldName, FilterFieldValue);
        } else {
            formatted = String.format(templateNoFilter, FieldName, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        MAX_VALUE objMaxVal = App.getDatabaseClient().getAppDatabase().genericDao().getMaxValue(query);
        return objMaxVal.MAX_VAL;
    }

    public static String GetCurrentDateTime(final String outputFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat, Locale.getDefault());
        Date dateObj = new Date();
        return simpleDateFormat.format(dateObj);
    }

    public static long convertToLong(String value, int defValue) {
        //String userdata = /*value from gui*/
        long val;
        try {
            val = Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    public static int convertToInt(String value, int defValue) {
        //String userdata = /*value from gui*/
        int val;
        try {
            val = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    public static double convertToDouble(String value, double defValue) {
        //String userdata = /*value from gui*/
        double val;
        try {
            val = Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    public static float convertToFloat(String value, float defValue) {
        //String userdata = /*value from gui*/
        float val;
        try {
            val = Float.parseFloat(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    public static String convertToString(Object value) {
        String valueString = "";
        try {
            valueString = value.toString();
        } catch (Exception ex) {
            valueString = "";
        }
        return valueString;
    }

    public static String PadTextString(String Text, int Len, AppConstants.PadDirection PrintDirection) {
        if (Text == null) Text = "";
        switch (PrintDirection) {
            case idCenter: {
                int spaces = Len - Text.length();
                int padLeft = spaces / 2 + Text.length();
                String Temp1 = PadLeft(Text, padLeft);
                String Temp2 = PadRight(Temp1, Len);
                return Temp2;
                //break;
            }
            case idLeft: {
                return PadLeft(Text, Len);
                //break;
            }
            case idRight: {
                return PadRight(Text, Len);
                //break;
            }
            default:
                return "";
            //break;
        }
    }

    public static String PadLeft(String Data, int TotalLength) {

        if (Data.length() >= TotalLength) return Data;

        String replicated = ReplicateChar(' ', TotalLength);
        //String unpadded = "12345";

        String padded = replicated.substring(Data.length()) + Data;
        return padded;
    }

    public static String PadRight(String Data, int TotalLength) {

        String replicated = ReplicateChar(' ', TotalLength);
        //String unpadded = "12345";
//        String padded =  (Data   + replicated).substring(Data.length());
        String padded = (Data + replicated).substring(0, TotalLength);
        return padded;


    }

    public static String ReplicateChar(char what, int Len) {
        //String repeated = Char.repeat(3);
        return repeat(what, Len);
    }

    public static String repeat(char what, int howmany) {
        char[] chars = new char[howmany];
        Arrays.fill(chars, what);
        return new String(chars);
    }

    public static String getFormattedDate(String dateValue, String FromDateFormat, String ToDateFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FromDateFormat);
        Date _date = null;
        try {
            _date = dateFormat.parse(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(_date.toString());


        // print the Date in year month and date


        SimpleDateFormat targetFormat = new SimpleDateFormat(ToDateFormat);
        return targetFormat.format(_date);
    }

    public static Date convertStringToDate(final String dateValue, final String dateFormat) {
        Date returnDate = null;
        try {
            ParsePosition position = new ParsePosition(0);
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            returnDate = sdf.parse(dateValue, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnDate;
    }

    public static List<String> SplitAsChunks(String text, int size) {
        // Give the list the right capacity to start with. You could use an array
        // instead if you wanted.
        List<String> ret = new ArrayList<String>((text.length() + size - 1) / size);

        for (int start = 0; start < text.length(); start += size) {
            ret.add(text.substring(start, Math.min(text.length(), start + size)));
        }
        return ret;
    }

    public static int GetRowsCount(String TableName, boolean useFilter, String FieldName, String FieldValue) {
        String templateHasFilter = "SELECT COUNT(1) AS TOTAL_COUNT FROM %s WHERE %s = '%s'";
        String templateNoFilter = "SELECT COUNT(1) AS TOTAL_COUNT FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, TableName, FieldName, FieldValue);
        } else {
            formatted = String.format(templateNoFilter, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCont = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);

        return objCont.TOTAL_COUNT;
    }

    public static int GetRowsCount(String TableName, boolean useFilter, String FieldName, long FieldValue) {
        String templateHasFilter = "SELECT COUNT(1) AS TOTAL_COUNT FROM %s WHERE %s = %d";
        String templateNoFilter = "SELECT COUNT(1) AS TOTAL_COUNT FROM %s ";
        String formatted = "";

        if (useFilter) {
            formatted = String.format(templateHasFilter, TableName, FieldName, FieldValue);
        } else {
            formatted = String.format(templateNoFilter, TableName);
        }

        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCont = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);

        return objCont.TOTAL_COUNT;
    }

    private static CustomResult CheckForPendingUpload(String TableName, String CheckFieldName, String FriendlyName) {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;

        String strTemplate = "SELECT COUNT(1) AS TOTAL_COUNT FROM %s WHERE %s = 0";
        String formatted = "";

        formatted = String.format(strTemplate, TableName, CheckFieldName);

        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCont = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        int cnt = objCont.TOTAL_COUNT;
        if (cnt > 0) {
            objResult.errCode = "E";
            objResult.errMessage = "Transaction Pending For Upload in " + FriendlyName;
            objResult.isSuccess = false;
        }
        return objResult;
    }

    public static CustomResult CheckForPendingUpload() {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;


        try {
            objResult = Utils.CheckForPendingUpload("TAGGED_SERIAL_NOS", "UPLOADED", "Tagging");
            if (!objResult.isSuccess) {
                return objResult;
            }
            objResult = Utils.CheckForPendingUpload("MOVED_SERIAL_NOS", "UPLOADED", "Transfer");
            if (!objResult.isSuccess) {
                return objResult;
            }
            objResult = Utils.CheckForPendingUpload("AUDITED_SERIAL_NOS", "UPLOADED", "Audit");
            if (!objResult.isSuccess) {
                return objResult;
            }
        } catch (Exception ex) {
            objResult.errCode = "E";
            objResult.errMessage = ex.getMessage();
            objResult.isSuccess = false;
        }
        return objResult;

    }

    public static CustomResult DeleteAllTransTable() {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;

        GenericDao data = App.getDatabaseClient().getAppDatabase().genericDao();
//        try {
//            data.deleteAllMasters(AppConstants.SITES);
//
//        } catch (Exception ex) {
//            objResult.errCode = "E";
//            objResult.errMessage = ex.getMessage();
//            objResult.isSuccess = false;
//        }
        return objResult;

    }

    public static CustomResult DeleteAllMastersTable() {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;

        GenericDao data = App.getDatabaseClient().getAppDatabase().genericDao();
        try {
//            data.deleteAllTaggedSerialNos();
//            data.deleteAllMovementInstructions();
//            data.deleteAllMovedSerialNos();
//            data.deleteAllAuditInstructions();
//            data.deleteAllAuditSerialNos();
//            data.deleteAllUser();

        } catch (Exception ex) {
            objResult.errCode = "E";
            objResult.errMessage = ex.getMessage();
            objResult.isSuccess = false;
        }
        return objResult;

    }

    private static boolean hasExternalSDCard() {
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
                return true;
        } catch (Throwable ignored) {
        }
        return false;
    }

    public static void GetExternalSDCardPath(Context context) {
        if (hasExternalSDCard()) {
            try {
                File[] files = context.getExternalFilesDirs(null);
                if (files == null) return;

                String sdPath = "";

                // Honeywell has IPSM Card
                for (File file : files) {
                    String currPath = file.getAbsolutePath();
                    if (!currPath.contains("emulated") && !currPath.contains("IPSM")) {
                        sdPath = currPath.substring(0, currPath.indexOf("Android/"));
                        break;
                    }
                }

                if (sdPath.isEmpty()) {
                    //no external sd card, so set to Downloads folder
                    EXTERNAL_SD_PATH = AppConstants.NO_EXT_PATH;
                    return;
                }
                ;
                File sdcard = new File(sdPath);
                if (sdcard.exists()) {
                    String value = sdcard.getAbsolutePath();
                    EXTERNAL_SD_PATH = value.endsWith("/") ? value : value.concat("/");
                }
            } catch (Exception e) {
                Log.d("SDPath", e.toString());
            }
        }
    }

    public synchronized static String getValue(Context context, String PREF_KEY, Boolean createRandom) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String uniqueID = sharedPrefs.getString(PREF_KEY, null);
        if ((uniqueID == null || uniqueID.length() == 0) && createRandom) {
            uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_KEY, uniqueID);
            editor.apply();
        }
        if (uniqueID == null) {
            uniqueID = "";
        }
        return uniqueID;
    }

    public synchronized static boolean SetValue(Context context, String PREF_KEY, String value) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREF_KEY, value);
        editor.apply();
        return true;
    }

}
